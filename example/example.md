# Lorem ipsum

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at fermentum erat, vitae gravida diam. Nunc ac lacus lectus. Nulla ultrices semper massa, eget suscipit quam aliquam mattis. Duis tristique neque in lectus pellentesque consequat. Maecenas non tincidunt risus. Pellentesque interdum tristique risus eget rhoncus. Pellentesque gravida mauris vel imperdiet pellentesque. Sed tincidunt ultricies risus, id euismod nisi euismod nec. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce at luctus est. Aliquam vitae sapien tempor, ullamcorper leo vel, sollicitudin massa. Phasellus ultricies mi ac tincidunt tempor. In hac habitasse platea dictumst. Phasellus ultricies porttitor est, ac porta quam mattis sit amet. Curabitur odio odio, consequat ac nisi et, consequat elementum nisl.

___

1. Lorem ipsum dolor sit ~~Strike through this text.~~
2. Consectetur adipiscing elit
3. Integer molestie lorem at _rendered as italicized text_
4. Facilisis in pretium nisl aliquet
5. Nulla volutpat aliquam velit
6. Faucibus porta lacus fringilla vel
7. Aenean sit amet erat nunc
8. Eget porttitor lorem

## Proin at dictum libero

Proin at dictum libero. Phasellus magna leo, **rendered as bold text** interdum vitae justo vitae,

### blockquote

> **Fusion Drive** combines a hard drive with a flash storage (solid-state drive) and presents it as a single logical volume with the space of both drives combined.

---

+ list item
+ list item
  - sub list item
  - sub list item
  - sub list item
+ list item
+ list item
+ list item
+ list item

+ _italic_
+ **bold**
+ ~~strike through~~

Proin at dictum libero. Phasellus magna leo, **rendered as bold text** interdum vitae justo vitae, dictum pretium lacus. In hac habitasse platea dictumst. Fusce pharetra malesuada ultricies. Integer ultricies, felis id imperdiet interdum, dui nibh mollis quam, et dapibus risus ante vel dui. Nunc cursus rhoncus augue, vel cursus diam consectetur a. Donec sed lacus sed nisi fringilla volutpat a vel neque. Ut dui sem, molestie non lobortis vitae, dapibus nec nibh. Nam consequat mi arcu, rhoncus malesuada est consectetur sed.

***

""Proin at dictum libero. Phasellus magna leo, **rendered as bold text** interdum vitae justo vitae""

---

example@example.com

[Assemble](http://assemble.io)[_blank]

[Call me](tel:123456)

[Upstage](https://github.com/upstage/ "Visit Upstage!")

> **Fusion Drive** combines a hard drive with a flash storage (solid-state drive) and presents it as a single logical volume with the space of both drives combined.

***

A code block

```php
<?php
	//include mkdown php class
	include 'mkdown.php';
	
	//new mkdown class instance
	$mkdown = new mkdown();
	
	//process example.md file contents and echo result 
	echo $mkdown->process(file_get_contents('example.md'))->result;
?>
```

![Minion](http://octodex.github.com/images/minion.png "What the hell?")