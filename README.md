# Markdown PHP

A simple markdown parser class for PHP

## Quick Usage

Simple example

```php
<?php
	//include mkdown php class
	include 'mkdown.php';
	
	//new mkdown class instance
	$mkdown = new mkdown();
	
	//process example.md file contents and echo result 
	echo $mkdown->process(file_get_contents('example.md'))->result;
?>
```

### Compatibility:

1. Headings
2. Horizontal Rules
3. Links (email, link with target, standard link)
4. Emphasis (bold, italic, strike through)
5. Quotes
6. Block quote
7. Lists (numerical and standard)
8. Paragraphs

### Implementation Todo:

1. Sub lists