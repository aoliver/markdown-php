<?php
/*
    Mkdown-PHP
    Copyright (C) 2018 Alex Oliver

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    @version: 1.0.2
    @environment: Development
    @author: Alex Oliver
    @Repo: https://bitbucket.org/aoliver/markdown-php


    v1.0.2 Log

    . Code blocks
    . Corrected issue detecting email addresses
    . Added keep comments param to class

*/
    class mkdown{
        
        var $result = '',
        $processed_array = [],
        $lists = [],
        $content_blocks = [],
        $keep_comments = true;

        function __construct(string $text = ''){
            $this->result = (strlen($text) > 0 ? $this->process($text) : false);
        }

        public function process(string $text = ''){
            if($this->result = (strlen($text) > 0 ? trim($text) : false)){
                
                //remove comments
                $this->result = $this->seek_code_block(($this->keep_comments ? $this->result : preg_replace('/<!--[^>]*-->/', '', $this->result)));

                //line breaks
                foreach(explode("\n\n", $this->result) as $this_line){
                    $this->processed_array[] = $this->process_line($this_line);
                }

                //set processed string
                $this->result = $this->process_code_block(implode("\n\n", $this->processed_array));

                //return this class instance
                return $this;
            }
        }

        private function process_line(string $this_line = ''){
            
            $break_rule = true;
            $is_paragraph = true;

            //headings
            if(preg_match_all('/^#+/', $this_line, $heading_count)){
                $heading_number = count(explode('#', $heading_count[0][0])) -1;
                $this_line = preg_replace('/>(\s)+/', ">", preg_replace('/^#+(.*)/', "<h$heading_number>$1</h$heading_number>", $this_line));
            } else {

                //horizontal rules
                $this_line = preg_replace('/^---|___|\*\*\*/', '<hr>', $this_line);

                //images
                $this_line = preg_replace('/!\[([^\]]+)\]\((.*)\s"(.*)"\)/', "<img src=\"$2\" alt=\"$1\" title=\"$3\"/>", $this_line);
                $this_line = preg_replace('/(!\[)(.*?)(\]\()(.+?)(\))/', "<img src=\"$4\" alt=\"$2\" />", $this_line);

                //a links: email, link with target, standard link
                $this_line = preg_replace('/([\w-.]+)@((?:[\w]+.)+)([a-zA-Z]{2,4})/', "<a href=\"mailto:$0\">$0</a>", $this_line);
                $this_line = preg_replace('/\[([^\]]+)\]\(([^)]+)\)\[([^\]]+)\]/', "<a href=\"$2\" target=\"$3\">$1</a>", $this_line);
                $this_line = preg_replace('/\[([^\]]+)\]\((.*)\s"(.*)"\)/', "<a href=\"$2\" title=\"$3\">$1</a>", $this_line);
                $this_line = preg_replace('/\[([^\]]+)\]\(([^)]+)\)/', "<a href=\"$2\">$1</a>", $this_line);

                //emphasis: bold, italic, strike through
                $this_line = preg_replace('/_(.*?)_/', "<em>$1</em>", $this_line);
                $this_line = preg_replace('/\*\*(.*?)\*\*/', "<strong>$1</strong>", $this_line);
                $this_line = preg_replace('/~~(.*?)~~/', "<s>$1</s>", $this_line);
                
                //quotes
                $this_line = preg_replace('/""(.*?)""/', "<q>$1</q>", $this_line);

                //block quote
                $this_line = preg_replace('/^>\s(.*)/', "<blockquote><p>$1</p></blockquote>", $this_line);

                //lists
                $number_list = [];
                $list_type = 'ul';
                
                if(preg_match_all('/^[\d]+./', $this_line, $this_number_list)){
                    //number
                    $is_paragraph = false;
                    $break_rule = false;
                    $list_type = 'ol';

                    foreach(explode("\n", $this_line) as $this_number_line){
                        $number_list[] = '<li>'.trim(preg_replace('/^[\d]+./', '', $this_number_line)).'</li>';
                    }

                } elseif(preg_match_all('/^\+|^\*/', $this_line, $this_li_list)){
                    //list items
                    $is_paragraph = false;
                    $break_rule = false;

                    foreach(explode("\n", $this_line) as $this_li_line){
                        $number_list[] = '<li>'.trim(preg_replace('/^\+|^\*/', '', $this_li_line)).'</li>';
                    }
                }

                if(count($number_list) > 0){
                    $this_line = str_replace($this_line, "<$list_type>\n".implode("\n", $number_list)."\n</$list_type>\n", $this_line);
                }

                //paragraph
                $this_line = ($is_paragraph ? "<p>$this_line</p>" : $this_line);

                
                //clean up
                $this_line = str_replace(['<p><hr></p>', '<p></p>'], ['<hr>', ''], $this_line);

                //line breaks
                $this_line = ($break_rule ? str_replace("\n", "<br>", $this_line) : $this_line);
            }

            return $this_line;
            
        }

        private function seek_code_block(string $the_content = ''){
            //seek
            if(preg_match_all('/\`\`\`(.*?)\n(.*?)\`\`\`/s', $the_content, $content_blocks, PREG_SET_ORDER)){
                foreach($content_blocks as $this_block){
                    //bookmark
                    $this->content_blocks[] = $this_block[2];
                    $the_content = str_replace($this_block[0], '[content_block:'.(count($this->content_blocks) - 1).']', $the_content);
                }
            }
            return $the_content;
        }

        private function process_code_block(string $the_content = ''){

            foreach($this->content_blocks as $key => $block_content){
                //format
                $block_content = str_replace(['<', '>'], ['&lt;', '&gt;'], $block_content);

                //replace
                $the_content = str_replace('[content_block:'.$key.']', '<pre><code>'.$block_content.'</code></pre>', $the_content);
            }

            return $the_content;
        }
    }
?>